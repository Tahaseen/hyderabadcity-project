import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { GalleryComponent } from './gallery/gallery.component';
import { PlacetovisitComponent } from './placetovisit/placetovisit.component';
import { ShoppingComponent } from './shopping/shopping.component';

const routes: Routes = [{path:'aboutus',component:AboutusComponent},
{path:'contactus',component:ContactusComponent},
{path:'gallery',component:GalleryComponent},
{path:'placetovisit',component:PlacetovisitComponent},
{path:'shopping',component:ShoppingComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
